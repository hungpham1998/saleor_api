package com.saleor.saleor_api.service;

import com.saleor.saleor_api.dto.DtoOrderDetail;
import com.saleor.saleor_api.dto.DtoProduct;
import com.saleor.saleor_api.dto.DtoProductCatogories;
import com.saleor.saleor_api.dto.DtoProductProperties;
import com.saleor.saleor_api.mapper.MapperProduct;
import com.saleor.saleor_api.mapper.MapperProductCatogories;
import com.saleor.saleor_api.mapper.MapperProductProperties;
import com.saleor.saleor_api.repo.*;
import com.saleor.saleor_api.table.*;
import com.saleor.saleor_api.utils.filterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;
import java.util.*;

@Service
public class SerProduct {
    @Autowired
    RepoProduct repoProduct;
    @Autowired
    RepoProductCatogories repoProductCatogories;

    @Autowired
    RepoProductProperties repoProductProperties;
    @Autowired
    SerProductCatogories serProductCatogories;
    @Autowired
    SerProductProperties serProductProperties;
    @Autowired
    RepoUnit repoUnit;
    @Autowired
    RepoWareHouse repoWareHouse;
    @Autowired
    MapperProduct mapperProduct;
    @Autowired
    MapperProductProperties mapperProductProperties;
    @Autowired
    MapperProductCatogories mapperProductCatogories;

    public List<DtoProduct> getDtoProductPage(Pageable pageable){
        List<Product> products = repoProduct.findAllBy(pageable);
        List<DtoProduct> dtoProducts = new ArrayList<DtoProduct>();
        for (Product item : products) {
            DtoProduct dtoProduct = mapperProduct.toDto(item);
            dtoProduct.setUnitsTitle(item.getUnits().getName());
            dtoProduct.setProductCategoriesName(item.getProductCatogories().getTitle());
            dtoProduct.setDtoProductProperties(new ArrayList<DtoProductProperties>(serProductProperties.getAllPropertiesByProductId(item)));
            dtoProducts.add(dtoProduct);
        }
        return dtoProducts;
    }

//    public Optional<DtoOrderDetail> FindByName(String title) {
//        return mapperProduct.toDtoList(repoProduct.findByName(title));
//    }


    public List<DtoProduct> getAllProductByProductCatogoriesId(ProductCatogories productCatogories) {
        List<Product> products = repoProduct.findAllByProductCatogories(productCatogories);
        List<DtoProduct> dtoProducts = new ArrayList<DtoProduct>();
        for (Product item : products) {
            DtoProduct dtoProduct = mapperProduct.toDto(item);
            dtoProduct.setUnitsTitle(item.getUnits().getName());
            dtoProduct.setProductCategoriesName(item.getProductCatogories().getTitle());
            dtoProduct.setDtoProductProperties(new ArrayList<DtoProductProperties>(serProductProperties.getAllPropertiesByProductId(item)));
            dtoProducts.add(dtoProduct);
        }
        return dtoProducts;
    }

    // check sku khi them san pham moi
    private boolean isSkuExist(String sku) {
        List<Product> ListSku = repoProduct.findBySku(sku);

        if (ListSku != null && ListSku.size() > 0) {
            return true;
        }
        return false;
    }
    // check xem don vi tinh co ton tai hay khong
    private Units checkProductUnit(String unit) {
        List<Units> productUnitOpt = repoUnit.findByName(unit);
        if (productUnitOpt.size() > 0) {
            return productUnitOpt.get(0);
        }
        return null;
    }
    // check xem loại sản phẩm có tồn tại không
    private ProductCatogories checkProductCatogories(String catogories) {
        List<ProductCatogories> productCatogories = repoProductCatogories.findByTitle(catogories);
        if (productCatogories.size() > 0) {
            return productCatogories.get(0);
        }
        return null;
    }
    // tao mới hoặc  thuộc tính produc properties
    public List<ProductProperties> createProperties(List<ProductProperties> productProperties){
        List<ProductProperties> newProperties = new ArrayList<ProductProperties>();
        if (productProperties.size() > 0 && productProperties != null) {
            for (ProductProperties productProperties1 : productProperties) {
                ProductProperties newItem = repoProductProperties.save(productProperties1);
                newProperties.add(newItem);
            }
        }
        return newProperties;
    }
    public Object InsSent(DtoProduct dtoProduct) {
        Map<String, Object> response = new HashMap<String,Object>();
        try {
            // check sku
            if (isSkuExist(dtoProduct.getSku())) {
                response.put("message", "mã sku đã tồn tại!!");
                response.put("success", false);
                return response;
            }
            // tìm tên đơn vị tính nêu có thì trả ra
            String unit = dtoProduct.getUnitsTitle();
            Units units = checkProductUnit(unit);
            if (units == null) {
                Units newUnits = new Units();
                newUnits.setName(unit);
                newUnits.setActive(true);
                units = repoUnit.save(newUnits);
            }
            // tìm loại sản phẩm nếu có thì trả ra
            String catogories = dtoProduct.getProductCategoriesName();
            ProductCatogories catogories1 = checkProductCatogories(catogories);
            if(catogories1 == null){
                response.put("success", false);
                return response;
            }
            Product product = new Product();
            product.setContent(dtoProduct.getContent());
            product.setCreatedDate(new Date());
            product.setBarCode(dtoProduct.getBarCode());
            product.setDescs(dtoProduct.getDescs());
            product.setName(dtoProduct.getName());
            product.setModifiedDate(new Date());
            product.setCreatedBy(dtoProduct.getCreatedBy());
            product.setModifiedBy(dtoProduct.getModifiedBy());
            product.setSku(dtoProduct.getSku());
            product.setPrice(dtoProduct.getPrice());
            product.setSalePrice(dtoProduct.getSalePrice());
            product.setQrCode(dtoProduct.getQrCode());
            product.setIsActive(dtoProduct.getIsActive());
            product.setQuantitySold(dtoProduct.getQuantitySold());
            product.setQuantityCurrent(dtoProduct.getQuantityCurrent());
            product.setImages(dtoProduct.getImages());
            product.setUnits(units);
            product.setProductCatogories(catogories1);
            Product coproduct = repoProduct.save(product);
            product.setId(coproduct.getId());
            List<DtoProductProperties> listProperties = dtoProduct.getDtoProductProperties();
            List<ProductProperties> newProperties = new ArrayList<>();
            if (listProperties.size() > 0 && listProperties != null) {
                for (DtoProductProperties item : listProperties) {
                    ProductProperties productProperties= new ProductProperties();
                    productProperties.setProduct_key(item.getProduct_key());
                    productProperties.setProduct_value(item.getProduct_value());
                    ProductProperties newItem = repoProductProperties.save(productProperties);
                }
            }
            response.put("data", dtoProduct);
            response.put("success", true);
            return response;
        } catch (Exception ex) {
            response.put("data", ex);
            response.put("success", false);
            return response;
        }
    }

}
