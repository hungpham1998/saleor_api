package com.saleor.saleor_api.service;

import com.saleor.saleor_api.dto.*;
import com.saleor.saleor_api.mapper.MapperOrders;
import com.saleor.saleor_api.repo.RepoCustomer;
import com.saleor.saleor_api.repo.RepoDetailOrder;
import com.saleor.saleor_api.repo.RepoOrders;
import com.saleor.saleor_api.repo.RepoProduct;
import com.saleor.saleor_api.table.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SerOrders {
    @Autowired
    RepoOrders repoOrders;
    @Autowired
    SerOrderDetail serOrderDetail;
    @Autowired
    RepoDetailOrder repoDetailOrder;
    @Autowired
    RepoProduct repoProduct;

    @Autowired
    RepoCustomer repoCustomer;
    @Autowired
    MapperOrders mapperOrders;
    public List<DtoOrders> GetAll()
    {
        return  mapperOrders.toDtoList(repoOrders.findAll());
    }

    public List<DtoOrders> getDtoOrderPage(Pageable pageable){
        List<Orders> orders = repoOrders.findAllBy(pageable);
        List<DtoOrders> dtoOrders = new ArrayList<DtoOrders>();
        for (Orders item : orders) {
            DtoOrders dtoOrder = mapperOrders.toDto(item);
//            System.out.printf("data"+ item);
            dtoOrder.setCustomer_id(item.getCustomer().getId());
            dtoOrder.setDtoOrderDetails(new ArrayList<DtoOrderDetail>(serOrderDetail.getAllOrderDetailByOrderId(item)));
            dtoOrders.add(dtoOrder);
        }
        return dtoOrders;
    }
    Map<String, Object> response = new HashMap<>();
    public Object  InsSent(DtoOrders dtoOrders){
        try{
            Orders orders = new Orders();

            orders.setCreatedBy(dtoOrders.getCreatedBy());

            orders.setCreatedDate(new Date());

            orders.setAddress(dtoOrders.getAddress());

            orders.setCash_money(dtoOrders.getCash_money());

            orders.setDiscount_money(dtoOrders.getDiscount_money());

            orders.setDistrict_id(dtoOrders.getDistrict_id());

            orders.setDiscount_percent(dtoOrders.getDiscount_percent());

            orders.setOrder_status(dtoOrders.getOrder_status());

            orders.setModified_by(dtoOrders.getModified_by());

            orders.setPaid_money(dtoOrders.getPaid_money());

            orders.setTotal_money(dtoOrders.getTotal_money());

            orders.setProvince_id(dtoOrders.getProvince_id());

            orders.setWard_id(dtoOrders.getWard_id());

            orders.setModifiedDate(new Date());

            Optional<Customer> customer = repoCustomer.findById(dtoOrders.getCustomer_id());

            if(!customer.isPresent()){
                response.put("success", false);
                return response;
            }

            Orders newOrder = new Orders();

            orders.setCustomer(customer.get());

            newOrder = repoOrders.save(orders);

            orders.setId(newOrder.getId());
            List<Product> products = repoProduct.findBy();
            List<DtoOrderDetail> orderDetails = dtoOrders.getDtoOrderDetails();
            for(DtoOrderDetail item : orderDetails){
                OrderDetail newOrderDetail = new OrderDetail();
                newOrderDetail.setMoney(item.getMoney());
                newOrderDetail.setPrice(item.getPrice());
                newOrderDetail.setQuantity(item.getQuantity());
                newOrderDetail.setWeight(item.getWeight());
                for(Product product: products){
                    if(item.getProduct_id().equals(product.getId())){
                        newOrderDetail.setProduct(product);
                    }
                }
                newOrderDetail.setOrders(newOrder);
                newOrderDetail = repoDetailOrder.save(newOrderDetail);
                item.setId(newOrderDetail.getId());
            }

           response.put("data", dtoOrders);
            response.put("success", true);
            return response;
        }
        catch(Exception e){
            response.put("success", false);
            response.put("mesager", e.getMessage());
            return response;
        }
    }

}
