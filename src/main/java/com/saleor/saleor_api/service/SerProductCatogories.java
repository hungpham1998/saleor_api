package com.saleor.saleor_api.service;

import com.saleor.saleor_api.dto.DtoProduct;
import com.saleor.saleor_api.dto.DtoProductCatogories;
import com.saleor.saleor_api.dto.DtoProductProperties;
import com.saleor.saleor_api.mapper.MapperProductCatogories;
import com.saleor.saleor_api.repo.RepoProductCatogories;
import com.saleor.saleor_api.table.*;
import com.saleor.saleor_api.utils.filterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SerProductCatogories {

    @Autowired
    RepoProductCatogories repoProductCatogories;

    @Autowired
    SerUser serUser;

    @Autowired
    SerProduct serProduct;

    @Autowired
    MapperProductCatogories mapperProductCatogories;
    public List<DtoProductCatogories> getdtoProductCatogoriesPage(Pageable pageable){
        List<ProductCatogories> productCatogories= repoProductCatogories.findAllBy(pageable);
        List<DtoProductCatogories> dtoProductCatogories= new ArrayList<>();
        for(ProductCatogories item:productCatogories){
            DtoProductCatogories dtoProductCatogories1= mapperProductCatogories.toDto(item);
            dtoProductCatogories1.setDtoProducts(new ArrayList<DtoProduct>(serProduct.getAllProductByProductCatogoriesId(item)));
            dtoProductCatogories.add(dtoProductCatogories1);
        }
        return dtoProductCatogories;
    }
    Map<String,Object> reponse = new HashMap<>();
    public Optional<ProductCatogories> GetByID(Long id)
    {
        return  repoProductCatogories.findById(id);
    }
    public Object save(ProductCatogories response){
        try{
            ProductCatogories newdata = repoProductCatogories.save(response);
            reponse.put("data", newdata);
            reponse.put("success", true);
            reponse.put("mesager", "ok");
            return reponse;
        }
        catch (Exception e){
            reponse.put("success", false);
            reponse.put("mesager", e.getMessage());
            return  reponse;
        }
    }

    public Object findById(Long id){
        try {
            Optional<ProductCatogories> opCatogories = repoProductCatogories.findById(id);
            Object data = filterObject.filter(opCatogories, "Khong tim thấy danh muc san pham ");
            return data;
        }
        catch (Exception e){
            reponse.put("success", false);
            reponse.put("mesager",e.getMessage());
            return reponse;
        }
    }

    public Object delete(Long id){
        try{
            Optional<ProductCatogories> opCatogories = repoProductCatogories.findById(id);
            if(!opCatogories.isPresent()){
                reponse.put("success", false);
                reponse.put("mesager","Khong tim thấy danh muc san pham ");
                return reponse;
            }
            repoProductCatogories.deleteById(id);
            reponse.put("success", true);
            reponse.put("mesager","xoa thanh cong");
            return reponse;
        }
        catch (Exception e){
            reponse.put("success", false);
            reponse.put("mesager",e.getMessage());
            return reponse;
        }
    }
}
